package com.hackrussia.rest.pojo.response;

import com.hackrussia.rest.status.Status;

/**
 * Created by amylniko on 26.07.2016.
 */
public class UserResponse {

    Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
