package com.hackrussia.rest.status;

/**
 * @author Ivan
 * @since 11.06.2016
 */
public enum Status {
    OK, ERROR;
}
