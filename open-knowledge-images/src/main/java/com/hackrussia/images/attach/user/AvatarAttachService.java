package com.hackrussia.images.attach.user;

import com.hackrussia.images.attach.AttachConstants;
import com.hackrussia.images.attach.AttachImageService;
import com.hackrussia.images.exception.AttachImageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.hackrussia.openknowledge.mongodb.enitites.User;
import com.hackrussia.openknowledge.mongodb.repos.UserRepository;
import com.hackrussia.rest.pojo.response.UserInformation;

/**
 * @author Ivan
 * @since 29.08.2016
 */
@Component(value = AttachConstants.AVATAR_ATTACH_SERVICE_BEAN_NAME)
public class AvatarAttachService implements AttachImageService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void attachImage(String referenceId, String imagePath) throws AttachImageException {
        String userName = ((UserInformation) SecurityContextHolder.getContext().getAuthentication().getDetails()).getUserName();
        if (!userName.equals(referenceId)) {
            // TODO find better solution
            throw new AttachImageException("You can't change not your own avatar!");
        }
        // TODO A. Mylnikov wtf with user creations?
        User userEntity = userRepository.findByLogin(userName);
        userEntity.setImagePath(imagePath);
        userRepository.save(userEntity);
    }

    @Override
    public void deleteAttachment(String referenceId, String imagePath) throws AttachImageException {
        // TODO implement
    }
}
