package com.hackrussia.openknowledge.services;

import com.hackrussia.rest.pojo.response.UserPojo;

/**
 * Service for updating and getting information about current user
 */
public interface UserService {

    /**
     * Updates current session user.
     * @param userPojo new information
     */
    void updateUser(UserPojo userPojo);

    /**
     *
     * @param userName
     * @param orderId
     */
    void thankUser(String userName, String orderId, String message);
}
