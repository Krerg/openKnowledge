package com.hackrussia.openknowledge.mapping;

import com.hackrussia.openknowledge.mongodb.enitites.Order;
import com.hackrussia.openknowledge.mongodb.enitites.OrderStatus;
import com.hackrussia.rest.pojo.OrderPojo;
import com.hackrussia.rest.pojo.response.UserInformation;
import org.mongodb.morphia.geo.GeoJson;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component(value = "orderPojoToEntityConverter")
public class OrderPojoToEntityConverter implements Converter<OrderPojo, Order> {

    @Override
    public Order convert(OrderPojo orderPojo) {
        Order orderEntity = new Order();
        orderEntity.setPosition(GeoJson.point(orderPojo.getGeolocation().getLatitude(),orderPojo.getGeolocation().getLongitude()));
        orderEntity.setOrderStatus(OrderStatus.NOT_INITED);
        orderEntity.setInformation(orderPojo.getOrderInfo().getInformation());
        orderEntity.setUserName(((UserInformation)SecurityContextHolder.getContext().getAuthentication()).getUserName());
        return orderEntity;
    }

}
