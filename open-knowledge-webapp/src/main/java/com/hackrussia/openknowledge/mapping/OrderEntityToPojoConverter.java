package com.hackrussia.openknowledge.mapping;

import com.hackrussia.openknowledge.mongodb.enitites.Order;
import com.hackrussia.rest.pojo.OrderPojo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by amylniko on 26.08.2016.
 */
@Component(value = "orderEntityToPojoConverter")
public class OrderEntityToPojoConverter implements Converter<Order, OrderPojo> {
    @Override
    public OrderPojo convert(Order order) {
        return null;
    }
}
