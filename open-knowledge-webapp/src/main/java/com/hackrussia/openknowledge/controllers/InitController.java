package com.hackrussia.openknowledge.controllers;

import com.hackrussia.openknowledge.services.OrderService;
import com.hackrussia.openknowledge.services.impl.UserServiceImpl;
import org.mongodb.morphia.geo.GeoJson;
import org.mongodb.morphia.geo.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackrussia.openknowledge.mongodb.enitites.Order;
import com.hackrussia.openknowledge.mongodb.enitites.OrderStatus;
import com.hackrussia.openknowledge.mongodb.enitites.User;
import com.hackrussia.openknowledge.mongodb.repos.UserRepository;


@RestController
@RequestMapping(value = "/init")
public class InitController {

	@Autowired
	private UserServiceImpl userService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public void initStartUsersAndOrders() {
		User user = new User("Germa", "0000", "+79118465234");
		userRepository.save(user);
		Point point = GeoJson.point(10.0,10.0);
		Order order = new Order("German", user.getPhoneNumber(), point, OrderStatus.NOT_INITED);
		orderService.saveOrder(order);
	}
}
